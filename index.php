<?php
    require __DIR__.'/vendor/autoload.php';

    $tg = new \Tg\Tg();
    $update = $tg->get_update_contents();

    $ext_id = $update->basic_params->chat_id;
    $msg_text = trim($update->basic_params->msg_text);

    $command = '';
    $command_params = '';
    if (preg_match("/^(\/\S+)(?:\s+(.*))?$/", $msg_text, $m)) {
        $command = $m[1];
        if (!empty($m[2]))
            $command_params = trim($m[2]);

        $command = preg_replace("/@.*$/", '', $command);
    }


    $state_storage = \TgState\TgStateFactory::get_storage($ext_id);
    $state = $state_storage->get_user_state();

    $controller_class = '';
    if (preg_match("/^\/start$/", $command))
        $controller_class = 'Start';
    elseif (preg_match("/^\/about$/", $command))
        $controller_class = 'About';
    elseif (preg_match("/^https?:\/\//", $msg_text))
        $controller_class = 'AddLinks';
    elseif (preg_match("/^\/del_ul\d+$/", $command))
        $controller_class = 'DeleteLink';
    elseif ($command == '/list')
        $controller_class = 'ListLinks';
    elseif ($command == '/sites')
        $controller_class = 'ListSites';
    elseif (preg_match("/^\/lang_\w+$/", $command))
        $controller_class = 'SetLang';
    else
        $controller_class = 'Unknown';
    
    $controller_class = '\Controller\Controller'.$controller_class;
    $controller = new $controller_class($update, $state);
    $result = $controller->handle();

    $state_storage->set_user_state(
        $result->state->state,
        $result->state->data
    );

    if (is_string($result->answer)) {
        $tg->answer_text($ext_id, $result->answer);
    }
