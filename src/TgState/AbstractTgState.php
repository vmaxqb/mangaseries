<?php
    namespace TgState;

    /**
     * Managing telegram-user states
     */
    abstract class AbstractTgState
    {
        /**
         * @var mixed Database connection
         */
        protected $con;

        /**
         * @var int Telegram user_id
         */
        protected $ext_id;

        /**
         * Makes a database connection and saves ext_id
         */
        public function __construct(int $ext_id)
        {
            $this->con = \DbConnection\DbConnection::connect();
            $this->ext_id = $ext_id;
        }

        /**
         * Getting state of a user
         * @return object|null State. It should be either null or have these parameters:
         *      state - (string) state name
         *      data - (object | empty string) state data
         */
        abstract public function get_user_state(): ?object;

        /**
         * Setting state of a user
         * @param string $state State name
         * @param string|object $state_data State data. Empty string or object data
         */ 
        abstract public function set_user_state(string $state='', $state_data=''): void;
    }
