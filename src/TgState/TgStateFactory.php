<?php
    namespace TgState;

    use \Config\Config;

    /**
     * A factory, that returns a TgState
     */
    final class TgStateFactory
    {
        use \Traits\StaticClass;

        /**
         * @var AbstractStorage A cache for a TgState object. It will not be created twice by this factory
         */
        private static $cache;

        /**
         * Gets an object of AbstractTgState, that will used for getting and setting states
         * @param int $ext_id Telegram user_id
         * @return AbstractTgState
         */
        public static function get_storage(int $ext_id): ?AbstractTgState
        {
            if (empty(self::$cache)) {
                $db_connection_string = Config::config('DB_CONNECTION', 'mysql');

                if ($db_connection_string == 'mysql')
                    self::$cache = new TgStateMysql($ext_id);
                else
                    self::$cache = null;
            }

            return self::$cache;
        }
    }
