<?php
    namespace TgState;

    class TgStateMysql extends AbstractTgState
    {
        public function get_user_state(): ?object
        {
            $sql = "
                select
                    state,
                    data
                from
                    tg_user_state
                where
                    ext_id = ? and
                    dt_message > now() - interval 30 day
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('i', $this->ext_id);
            $stmt->execute();

            foreach (\DbConnection\DbConnection::fetch_results($stmt) as $row) {
                if (!empty($row->data))
                    $row->data = @json_decode($row->data);
                return $row;
            }

            return null;
        }


        public function set_user_state(string $state='', $state_data=''): void
        {
            $sql = "
                replace into tg_user_state
                (
                    ext_id,
                    state,
                    data
                )
                values
                (
                    ?,
                    ?,
                    ?
                )
            ";

            $stmt = $this->con->prepare($sql);
            $encoded_state_data = @json_encode($state_data);
            $stmt->bind_param('iss', $this->ext_id, $state, $encoded_state_data);
            $stmt->execute();
        }
    }
