<?php
    namespace Config;

    /**
     * This class reads config-file .env
     * .env file may look like its from Laravel framework, but it actially an ini-file
     * Did not get Laravel's version of env()-function, because of KISS and NIH
     */
    class Config
    {
        use \Traits\StaticClass;

        /**
         * @var array It's a cache for already read config
         */
        private static $config;

        /**
         * Reads config-file .env if it wasn't read before, gets needed param value
         * @param string $param Needed param
         * @param string|array $default If a param is not found, this value will be returned
         * @return string|array Returns a value definded by this param. Or $default if param was not found
         */
        public static function config(string $param, $default='')
        {
            if (empty(self::$config))
                self::$config = parse_ini_file(__DIR__.'/../../.env');

            if (isset(self::$config[$param]))
                return self::$config[$param];

            if (!empty($default))
                return $default;

            return '';
        }
    }
