<?php
    namespace Storage;

    /**
     * This class is for storing ang getting data.
     * It's like a model, but just functions
     * Each child class should correspond a certain storage engine
     */
    abstract class AbstractStorage
    {
        /**
         * @var connection_id Connection handle to the database
         */
        protected $con;

        public function __construct()
        {
            $this->con = \DbConnection\DbConnection::connect();
        }

        /**
         * Reconnectiong connection lost
         * @param bool $force True if we should reconnect anyway
         */
        public function con_ping(bool $force=false): void
        {
            $this->con = \DbConnection\DbConnection::ping($force);
        }

        /**
         * Gets user_id by ext_id, creates it if it does not exist
         * @param string $platform Platform, user_ext_id belongs to
         * @param string $user_ext_id Id of the user on external platform
         * @return int Local user_id
         */
        abstract public function get_user_id(string $platform, string $user_ext_id): int;

        /**
         * Gets user data by ext_id, creates it if it does not exist
         * @param string $platform Platform, user_ext_id belongs to
         * @param string $user_ext_id Id of the user on external platform
         * @return object User data
         */
        abstract public function get_user(string $platform, string $user_ext_id): object;

        /**
         * Gets all links, user is watching
         * @param int $user_id Local user_id
         * @return array All user link data. Every element is an object of params:
         *      link_id - (int) An id of the link
         *      link - (string) The link
         *      last_version_mark - (object) All the data got by the last parsing of that link
         *          like naming, last episode, etc
         */
        abstract public function get_user_links(int $user_id): array;

        /**
         * Adds a link to user's watchlist
         * Does not add a duplicate link
         * @param int $user_id Local user_id
         * @param int $link_id Local link_id, got by get_link_id function
         */
        abstract public function add_user_link(int $user_id, int $link_id): void;

        /**
         * Deletes a link from user's watchlist
         * @param int $user_id Local user_id
         * @param int $link_id Local link_id, got by get_link_id function
         */
        abstract public function del_user_link(int $user_id, int $link_id): void;

        /**
         * Gets local link_id by link, creates it (if $create=true) if it does not exist
         * @param string $link The link to get id of
         * @param bool $create Wether to create a record or not
         * @return int|false Link_id of the link. If the link is not found and create = false, returns false
         */
        abstract public function get_link_id(string $link, bool $create=true): int;

        /**
         * Gets all links that are about to check, i.e. presents in at least one user's wacthlist
         * @return array of link data. Every element is an object:
         *      link_id - (int) id of the link
         *      link - (string) the link to check
         *      last_version_hash - (string) the hash of last version's data
         *      ts_since_update - int|null seconds since last update (last episode). null if there was no updates yet
         *      ts_since_check - int|null seconds since last check for updates. null if there was no checks yet
         */
        abstract public function get_used_links(): array;

        /**
         * Updates link data in the storage when the site was updated
         * @param int $link_id Id of the link for update
         * @param object $last_version_mark Last version's data, returned by Parser\AbstractParser::get_latest_version
         * @param string $last_version_hash The hash of the last version, returned by Parser\AbstractParser::get_latest_version
         */
        abstract public function renew_link(int $link_id, object $last_version_mark, string $last_version_hash): void;

        /**
         * Updates link data when the site was checked but not updated
         * @param int $link_id Id of the link we have checked
         */
        abstract public function check_link(int $link_id): void;

        /**
         * Gets all users and their links that are not up to date.
         * Basically for further notification.
         * @param string $platform What users should we fetch
         * @return array Every element is an object:
         *      user_id - (int) local user_id
         *      ext_id - (int) external user_id on the platform
         *      link_id - (int) id of a link that is not up to date for this user
         *      link - (string) the link that was updated
         *      last_version_mark - (object) the data of last version parsion. See Parser\AbstractParser::get_latest_version for details
         *      completed - (int 1|0) was the serie completed
         */
        abstract public function get_nonrenewed_user_links(string $platform): array;

        /**
         * Updates the hash of the link, implying we have notified the user about the link update
         * @param int $user_id Local user_id
         * @param int $link_id Local link_id, got by get_link_id function
         */
        abstract public function renew_user_link(int $user_id, int $link_id): void;

        /**
         * Sets the language to the user
         * @param int $user_id Local user_id
         * @param string $lang Language to set
         */
        abstract public function set_user_lang(int $user_id, string $lang): void;

        /**
         * Adds a link "suggestion". If a site is not supported, the link is to be added to "suggestions"
         * @param int $user_id Local user_id
         * @param string $link Link the user suggests
         */
        abstract public function add_suggest(int $user_id, string $link): void;

        /**
         * Fetches data about a link. To be used by parsers.
         *
         * @param string $link
         * @return object|null Null if the link does not exists yet. Following data otherwise:
         * {
         *   link_id,
         *   last_version_hash,
         *   last_version_ts
         * }
         */
        abstract public function get_link_data(string $link): ?object;
    }
