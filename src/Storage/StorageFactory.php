<?php
    namespace Storage;

    use \Config\Config;

    /**
     * This is a StorageFactory, gets a storage, based on config
     */
    final class StorageFactory
    {
        use \Traits\StaticClass;

        /**
         * @var AbstractStorage A cache for a storage object. It will not be created twice by this factory
         */
        private static $cache;

        /**
         * Gets a storage object, based on config
         * @return AbstractStorage
         */
        public static function get_storage(): ?AbstractStorage
        {
            if (empty(self::$cache)) {
                $db_connection_string = Config::config('DB_CONNECTION', 'mysql');
                if ($db_connection_string == 'mysql')
                    self::$cache = new StorageMysql();
                else
                    self::$cache = null;
            }

            return self::$cache;
        }
    }
