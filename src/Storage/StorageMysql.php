<?php
    namespace Storage;

    use \DbConnection\DbConnection;

    class StorageMysql extends AbstractStorage
    {
        public function get_user_id(string $platform, string $user_ext_id): int
        {
            $sql = "
                select
                    u.user_id
                from
                    users u
                where
                    u.platform = ? and
                    u.ext_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ss', $platform, $user_ext_id);
            $stmt->execute();
            foreach (DbConnection::fetch_results($stmt) as $row)
                return $row->user_id;

            $sql = "
                insert into users
                (
                    platform,
                    ext_id
                )
                values
                (
                    ?,
                    ?
                )
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ss', $platform, $user_ext_id);
            $stmt->execute();

            return $stmt->insert_id;
        }


        public function get_user(string $platform, string $user_ext_id): object
        {
            $sql = "
                select
                    u.user_id,
                    u.tz_delta,
                    u.lang
                from
                    users u
                where
                    u.platform = ? and
                    u.ext_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ss', $platform, $user_ext_id);
            $stmt->execute();
            foreach (DbConnection::fetch_results($stmt) as $row)
                return $row;

            $sql = "
                insert into users
                (
                    platform,
                    ext_id
                )
                values
                (
                    ?,
                    ?
                )
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ss', $platform, $user_ext_id);
            $stmt->execute();

            return $this->get_user($platform, $user_ext_id);
        }


        public function get_user_links(int $user_id): array
        {
            $sql = "
                select
                    l.link_id,
                    l.link,
                    l.last_version_mark
                from
                    users u
                inner join
                    user_links ul
                    on u.user_id = ul.user_id
                inner join
                    links l
                    on ul.link_id = l.link_id
                where
                    u.user_id = ?
                order by
                    l.link
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('i', $user_id);
            $stmt->execute();
            $result = [];
            foreach (DbConnection::fetch_results($stmt) as $row) {
                $row->last_version_mark = @json_decode($row->last_version_mark);
                $result[] = $row;
            }
            return $result;
        }


        public function add_user_link(int $user_id, int $link_id): void
        {
            // checking if this link already added

            $sql = "
                select
                    ul.user_link_id
                from
                    user_links ul
                where
                    ul.user_id = ? and
                    ul.link_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ii', $user_id, $link_id);
            $stmt->execute();
            if (!empty(DbConnection::fetch_results($stmt)))
                return;
            
            // inserting if no
            $sql = "
                insert into user_links
                (
                    user_id,
                    link_id,
                    last_version_hash
                )
                select
                    ?,
                    l.link_id,
                    l.last_version_hash
                from
                    links l
                where
                    l.link_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ii', $user_id, $link_id);
            $stmt->execute();
        }


        public function del_user_link(int $user_id, int $link_id): void
        {
            $sql = "
                delete from
                    user_links
                where
                    user_id = ? and
                    link_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ii', $user_id, $link_id);
            $stmt->execute();
        }


        public function get_link_id(string $link, bool $create=true): int
        {
            $sql = "
                select
                    l.link_id
                from
                    links l
                where
                    l.link = ?
                order by
                    l.link_id
                limit
                    1
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('s', $link);
            $stmt->execute();
            foreach (DbConnection::fetch_results($stmt) as $row)
                return $row->link_id;

            if (!$create)
                return 0;

            $sql = "
                insert into links
                (
                    link
                )
                values
                (
                    ?
                )
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('s', $link);
            $stmt->execute();

            return $stmt->insert_id;
        }


        public function get_used_links(): array
        {
            $sql = "
                select distinct
                    l.link_id,
                    l.link,
                    l.last_version_hash,
                    unix_timestamp() - unix_timestamp(l.last_version_dt) ts_since_update,
                    unix_timestamp() - unix_timestamp(l.last_checked_dt) ts_since_check
                from
                    links l
                inner join
                    user_links ul
                    on ul.link_id = l.link_id
                where
                    l.completed = 0
                order by
                    l.link
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->execute();
            $result = [];
            return DbConnection::fetch_results($stmt);
        }


        public function renew_link(int $link_id, object $last_version_mark, string $last_version_hash): void
        {
            $completed = 0;
            if (!empty($last_version_mark->completed))
                $completed = 1;
            unset($last_version_mark->completed);

            $sql = "
                update
                    links l
                set
                    l.last_version_mark = ?,
                    l.last_version_hash = ?,
                    l.completed = ?,
                    l.last_version_dt = now(),
                    l.last_checked_dt = now()
                where
                    l.link_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $last_version_mark = @json_encode($last_version_mark, JSON_UNESCAPED_UNICODE);
            $stmt->bind_param('ssii', $last_version_mark, $last_version_hash, $completed, $link_id);
            $stmt->execute();
        }


        public function check_link(int $link_id): void
        {
            $sql = "
                update
                    links l
                set
                    l.last_checked_dt = now()
                where
                    l.link_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('i', $link_id);
            $stmt->execute();
        }


        public function get_nonrenewed_user_links(string $platform): array
        {
            // first of all renewing all user's links we've never renewed
            $sql = "
                update
                    user_links ul
                inner join
                    links l
                    on
                        ul.link_id = l.link_id and
                        l.last_version_hash is not null
                inner join
                    users u
                    on
                       u.user_id = ul.user_id and
                       u.platform = ?
                set
                    ul.last_version_hash = l.last_version_hash
                where
                    ul.last_version_hash is null
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('s', $platform);
            $stmt->execute();

            // now we get all user's nonrenewed links
            $sql = "
                select
                    u.user_id,
                    u.ext_id,
                    u.lang,
                    l.link_id,
                    l.link,
                    l.last_version_mark,
                    l.completed
                from
                    users u
                inner join
                    user_links ul
                    on u.user_id = ul.user_id
                inner join
                    links l
                    on
                        ul.link_id = l.link_id and
                        ul.last_version_hash != l.last_version_hash
                where
                    u.platform = ?
                order by
                    u.user_id,
                    l.link
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('s', $platform);
            $stmt->execute();

            $result = [];
            foreach (DbConnection::fetch_results($stmt) as $row) {
                $row->last_version_mark = @json_decode($row->last_version_mark);
                $result[] = $row;
            }
            return $result;
        }


        public function renew_user_link(int $user_id, int $link_id): void
        {
            $sql = "
                update
                    user_links ul
                inner join
                    links l
                    on
                        ul.link_id = l.link_id and
                        l.link_id = ?
                set
                    ul.last_version_hash = l.last_version_hash
                where
                    ul.user_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('ii', $link_id, $user_id);
            $stmt->execute();
        }


        public function set_user_lang(int $user_id, string $lang): void
        {
            $sql = "
                update
                    users u
                set
                    u.lang = ?
                where
                    u.user_id = ?
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('si', $lang, $user_id);
            $stmt->execute();
        }


        public function add_suggest(int $user_id, string $link): void
        {
            $sql = "
                insert into suggests
                (
                    user_id,
                    link
                )
                values
                (
                    ?,
                    ?
                )
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('is', $user_id, $link);
            $stmt->execute();
        }

        public function get_link_data(string $link): ?object
        {
            $sql = "
                select
                    l.link_id,
                    l.last_version_hash,
                    unix_timestamp(l.last_version_dt) last_version_ts
                from
                    links l
                where
                    l.link = ?
                order by
                    l.link_id
                limit
                    1
            ";

            $stmt = $this->con->prepare($sql);
            $stmt->bind_param('s', $link);
            $stmt->execute();
            foreach (DbConnection::fetch_results($stmt) as $row)
                return $row;
            return null;
        }
    }
