<?php
    namespace Traits;

    /**
     * This trait is used for classes that are not supposed to be created (using new command) by outside force
     * Used in singletons or classes with no non-static functions
     */
    trait StaticClass
    {
        private function __construct()
        {
        }

        private function __clone()
        {
        }
    }
