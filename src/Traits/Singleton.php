<?php
    namespace Traits;

    /**
     * Trait of a singleton template
     */
    trait Singleton
    {
        private static $instances = [];

        public static function getInstance(): self
        {
            $class = get_called_class();
            if (!isset(self::$instances[$class]))
                self::$instances[$class] = new $class();
            return self::$instances[$class];
        }

        use StaticClass;
    }
