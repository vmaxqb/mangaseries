<?php
    namespace DbConnection;

    use \Config\Config;
    
    /**
     * This class makes a connection to the database. And guarantees it will be
     * initialized just once.
     */
    class DbConnection
    {
        use \Traits\StaticClass;

        /**
         * @var connection_id Connection id. Variable type depends on database type.
         */
        private static $cache;
        
        /**
         * Making a connection to the database.
         * Connection parameters are determined in config-file.
         * Supports MySQL, Oracle, SQLite by now.
         * Connection id is saved to self::$cache.
         * @return connection_id Connection id. Variable type depends on database type.
         */
        public static function connect()
        {
            global $db_config;
            
            if (isset(self::$cache))
                return self::$cache;
            
            $db_type = Config::config('DB_CONNECTION', 'mysql');
            $persist = true; // let it be always persist
            $execute = Config::config('DB_EXECUTE');
            
            if ($db_type == 'oracle') {
                putenv('ORACLE_HOME='.Config::config('DB_ORACLE_HOME', '/usr/lib/oracle/11.2/client64'));
                putenv('LD_LIBRARY_PATH='.Config::config('DB_LD_LIBRARY_PATH', '/usr/lib/oracle/11.2/client64/lib'));
                
                if ($persist)
                    $dbh = oci_pconnect(
                        Config::config('DB_USERNAME'),
                        Config::config('DB_PASSWORD'),
                        Config::config('DB_HOST'),
                        Config::config('DB_LANG', 'CL8MSWIN1251')
                    );
                else
                    $dbh = oci_connect(
                        Config::config('DB_USERNAME'),
                        Config::config('DB_PASSWORD'),
                        Config::config('DB_HOST'),
                        Config::config('DB_LANG', 'CL8MSWIN1251')
                    );
                
                if ($dbh && !empty($execute)) {
                    foreach ($execute as $command) {
                        $command = trim($command);
                        if (empty($command))
                            continue;
                        
                        $stid = oci_parse($dbh, $command);
                        oci_execute($stid);
                        oci_free_statement($stid);
                    }
                }
            }
            elseif ($db_type == 'mysql') {
                $persist_prefix = '';
                if ($persist)
                    $persist_prefix = 'p:';
                
                $dbh = new \mysqli(
                    $persist_prefix.Config::config('DB_HOST', 'localhost'),
                    Config::config('DB_USERNAME'),
                    Config::config('DB_PASSWORD'),
                    Config::config('DB_DATABASE'),
                    Config::config('DB_PORT', 3306)
                );
                
                if ($dbh && !empty($execute)) {
                    foreach ($execute as $command) {
                        $command = trim($command);
                        if (empty($command))
                            continue;
                        
                        $dbh->query($command);
                    }
                }
            }
            elseif ($db_type == 'sqlite') {
                $dbh = new \SQLite3(Config::config('DB_HOST', ':memory:'));
                
                if ($dbh && !empty($execute)) {
                    foreach ($execute as $command) {
                        $command = trim($command);
                        if (empty($command))
                            continue;
                        
                        $dbh->exec($command);
                    }
                }
            }

            if ($dbh)
                self::$cache = $dbh;
            else
                self::$cache = false;
            
            return self::$cache;
        }

        /**
         * Pinging mysql connection
         * Reconnecting if connection is lost
         * Basically needed when this program is forked
         * MySQL support only. No reconnection done otherwise
         * @param bool $force Should we reconnect anyway? no is false
         * @return connection_id The same return value, as in connect() method
         */
        public static function ping(bool $force=false)
        {
            $db_type = Config::config('DB_CONNECTION', 'mysql');

            if ($db_type == 'mysql') {
                if (!$force && isset(self::$cache) && self::$cache->ping())
                    return self::$cache;
                self::$cache = null;
                self::connect();
            }

            return self::$cache;
        }


        /**
         * Fetching results from mysqli binded query.
         * Its a bit tricky, that why this function is for.
         * MySQL support only.
         * @param \mysqli_stmt $stmt Mysqli statement, returned my \mysqli::prepare function
         *      Expecting this statement is and executed select-query
         * @return array Array of rows. Each row is an object with parameters - query columns
         */
        public static function fetch_results(\mysqli_stmt $stmt): array
        {
            $db_type = Config::config('DB_CONNECTION', 'mysql');

            $result = [];
            if ($db_type == 'mysql') {
                $row = new \stdClass;
                $params = [];
                $meta = $stmt->result_metadata();
                while ($field = $meta->fetch_field()) {
                    $params[] = &$row->{$field->name};
                }
                //call_user_func_array([$stmt, 'bind_result'], $params); // pre 7.2
                $stmt->bind_result(...$params);

                while ($stmt->fetch()) {
                    /*
                        seems mad at first glance, but we undoing links here
                        $result[] = $row; would be wrong because $row values are links
                        and they will be rewritten on the next iteration
                        so we copy $row values to another variable $x
                    */
                    $x = new \stdClass;
                    foreach ($row as $key => $val)
                        $x->{$key} = $val;
                    $result[] = $x;
                }
            }

            return $result;
        }
    }
