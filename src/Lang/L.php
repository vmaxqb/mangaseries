<?php
    namespace Lang;

    /**
     * 
     */
    class L
    {
        use \Traits\StaticClass;

        /**
         * @var string Fallback language
         */
        const LANG_FALLBACK = 'en';

        /**
         * @var string Folder that contains translations
         */
        const LANG_DIR = 'translations';

        /**
         * @var array Translations [lang] => translation_list
         */
        private static $cache;

        /**
         * @var string Current language
         */
        private static $lang;

        /**
         * @var array All available languages
         */
        private static $available_langs;
        

        /**
         * Returns all available languages
         * @return array of language codes
         */
        public static function get_available_langs(): array
        {
            if (!isset(self::$available_langs)) {
                self::$available_langs = [];

                $dir = __DIR__.DIRECTORY_SEPARATOR.self::LANG_DIR.DIRECTORY_SEPARATOR;
                if ($handle = opendir($dir)) {
                    while (false !== ($file = readdir($handle))) {
                        if (is_file($dir.$file) && preg_match("/^(\w+)\.ini$/i", $file, $m))
                            self::$available_langs[] = $m[1];
                    }
                }
            }

            return self::$available_langs;
        }

        /**
         * Loads language translations to cache
         * @param string $lang Language to load. self::$lang is used by defualt
         */
        private static function load_lang(string $lang = ''): void
        {
            if (empty($lang))
                $lang = self::$lang;

            if (isset(self::$cache[$lang]))
                return;

            if (preg_match("/\W/", $lang))
                return;

            $file = __DIR__.DIRECTORY_SEPARATOR.self::LANG_DIR.DIRECTORY_SEPARATOR.$lang.'.ini';
            if (!is_file($file)) {
                self::$cache[$lang] = false;
                return;
            }

            self::$cache[$lang] = parse_ini_file($file);
        }

        /**
         * Sets current language
         * @param string $lang Languane to be set as current
         */
        public static function set_lang(string $lang): void
        {
            self::$lang = $lang;
        }

        /**
         * Translates the translated string, identified by key
         * @param string $key The key of translation
         * @param mixed $args Arguments, used in sprintf
         * @return string Translated string
         */
        public static function _(string $key, ...$args): string
        {
            if (empty(self::$lang))
                self::$lang = self::LANG_FALLBACK;

            self::load_lang();
            $cur_trans = self::$cache[self::$lang];
            if (isset($cur_trans[$key]))
                return sprintf($cur_trans[$key], ...$args);

            if (self::$lang == self::LANG_FALLBACK)
                return '';

            self::load_lang(self::LANG_FALLBACK);
            $cur_trans = self::$cache[self::LANG_FALLBACK];
            if (isset($cur_trans[$key]))
                return sprintf($cur_trans[$key], ...$args);

            return $key;
        }
    }
