<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserAnimelayer extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'animelayer.ru' => 'https://animelayer.ru/torrent/<ID>/',
            ],
            'type'  => 'torrent',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https?:\/\/(animelayer\.ru\/torrent\/)(\w{24})/", $link, $m))
                return 'https://'.$m[1].strtolower($m[2]).'/';

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim(html_entity_decode($dom->find('h1')[0]->text, ENT_QUOTES | ENT_HTML401));
            if (empty($name))
                return null;

            unset($dom);

            $mark = (object)[
                'name'  => $name,
            ];

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($name),
            ];
        }
    }
