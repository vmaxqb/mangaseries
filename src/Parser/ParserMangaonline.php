<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserMangaonline extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'manga-online.biz' => 'https://manga-online.biz/<NAME>.html',
            ],
            'type'  => 'manga',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https:\/\/(manga-online\.biz\/[^\/]+\.html)$/", $link, $m))
                return 'https://'.$m[1];

            if (preg_match("/^https:\/\/(manga-online\.biz\/[^\/]+)\/\d+\/\d+\/\d+\/$/", $link, $m))
                return 'https://'.$m[1].'.html';

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim($dom->find('h1.header')[0]->text);
            if (empty($name))
                return null;

            $first_name = trim(strtok($name, '/'));
            if (empty($first_name))
                $first_name = $name;

            $latest_chapter_name = @trim($dom->find('div#manga-chapter-container div.item div.content a.header')[0]->text);
            if (empty($latest_chapter_name))
                $latest_chapter_name = '';
            else
                $latest_chapter_name = trim(str_replace($first_name, '', $latest_chapter_name));

            $completed = false;
            if (trim($dom->find('div.meta')[0]->text) == 'закончен')
                $completed = true;

            unset($dom);

            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $latest_chapter_name,
            ];
            if ($completed)
                $mark->completed = 1;

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($latest_chapter_name),
            ];
        }
    }
