<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserMangaheretoday extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'mangahere.today' => 'http://mangahere.today/<NAME>',
            ],
            'type'  => 'manga',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https?:\/\/(mangahere\.today\/manga\/[^\/]+?)$/", $link, $m))
                return 'http://'.$m[1];

            if (preg_match("/^https?:\/\/(mangahere\.today)\/([^\/]+)\-chapter\-[\d\.]+(?:#.*)?$/", $link, $m))
                return 'http://'.$m[1].'/manga/'.$m[2];

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim($dom->find('h1.title-manga')[0]->text);
            if (empty($name))
                return null;
            $name = preg_replace("/ Manga$/", '', $name);

            $latest_chapter_name = @($dom->find('#examples .chapter-list h4 a')[0]->text());
            if (empty($name))
                return $latest_chapter_name;
            $latest_chapter_name = trim(str_replace($name, '', $latest_chapter_name));
            
            $desc_html = $dom->find('.description-update')[0]->innerhtml;
            $completed = !!preg_match("/^.*<span>Status: <\/span>\s*Completed/", $desc_html);

            unset($dom);

            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $latest_chapter_name,
            ];
            if ($completed)
                $mark->completed = 1;

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($latest_chapter_name),
            ];
        }
    }
