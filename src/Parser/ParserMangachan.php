<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserMangachan extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'manga-chan.me' => 'https://manga-chan.me/manga/<ID>-<NAME>.html',
                'yaoi-chan.me'  => 'http://yaoi-chan.me/manga/<ID>-<NAME>.html'
            ],
            'type'  => 'manga',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https:\/\/manga-chan.me\/manga\/+(\d+\-[\w\-]+\.html)$/", $link, $m))
                return 'https://manga-chan.me/manga/'.$m[1];

            if (preg_match("/^https?:\/\/yaoi-chan.me\/manga\/(\d+\-[\w\-]+\.html)$/", $link, $m))
                return 'http://yaoi-chan.me/manga/'.$m[1];

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim($dom->find('h1 a')[0]->text);
            if (empty($name))
                return null;

            $orig_name = trim(preg_replace("/\(.+$/", '', $name));

            // .manga on yaoi-chan, .manga2 on manga-chan
            $latest_chapter_name = @trim($dom->find('#tc_1 div.manga a, #tc_1 div.manga2 a')[0]->text);
            if (empty($latest_chapter_name))
                $latest_chapter_name = '';
            else {
                $latest_chapter_name = str_replace('&nbsp;', '', $latest_chapter_name);
                $latest_chapter_name = preg_replace("/\s+/", ' ', $latest_chapter_name);
                $latest_chapter_name = trim(str_replace($orig_name, '', $latest_chapter_name));
            }

            $completed = false;

            $items = $dom->find('table.mangatitle td.item2 h2 font');
            if (!empty($items)) {
                foreach ($items as $item) {
                    if ($item->text === 'перевод завершен') {
                        $completed = true;
                        break;
                    }
                }
            }

            unset($dom);

            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $latest_chapter_name,
            ];
            if ($completed)
                $mark->completed = 1;

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($latest_chapter_name),
            ];
        }
    }
