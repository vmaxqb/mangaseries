<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserMangahub extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'mangahub.io'     => 'https://mangahub.io/manga/<NAME>',
                'mangafox.fun'  => 'https://mangafox.fun/manga/<NAME>',
            ],
            'type'  => 'manga',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https?:\/\/((?:mangahub\.io|mangafox\.fun)\/manga\/[^\/]+)$/", $link, $m))
                return 'https://'.$m[1];

            if (preg_match("/^https?:\/\/(mangahub\.io|mangafox\.fun)\/chapter\/([^\/]+)\/[^\/]+$/", $link, $m))
                return 'https://'.$m[1].'/manga/'.$m[2];

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim($dom->find('h1')[0]->text);
            if (empty($name))
                return null;

            $latest_chapter_name = @($dom->find('ul.list-group a > .text-secondary')[0]->innertext);

            $completed = false;
            foreach ($dom->find('._3QCtP > div div') as $div) {
                if (
                    @($div->find('span._3SlhO')[0]->text) == 'Status' &&
                    @($div->find('span')[1]->text) == 'Completed'
                )
                    $completed = true;
            }

            unset($dom);

            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $latest_chapter_name,
            ];
            if ($completed)
                $mark->completed = 1;

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($latest_chapter_name),
            ];
        }
    }
