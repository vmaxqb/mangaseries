<?php
    namespace Parser;

    use Config\Config;
    use CurlClient\CurlClient;
    use Eljam\GuzzleJwt\JwtMiddleware;
    use Eljam\GuzzleJwt\Manager\JwtManager;
    use Eljam\GuzzleJwt\Persistence\SimpleCacheTokenPersistence;
    use Eljam\GuzzleJwt\Strategy\Auth\JsonAuthStrategy;
    use GuzzleHttp\Client;
    use GuzzleHttp\HandlerStack;
    use Symfony\Component\Cache\Adapter\FilesystemAdapter;
    use Symfony\Component\Cache\Psr16Cache;

    class ParserMangadex extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'mangadex.org'     => 'https://mangadex.org/title/<ID>/<NAME>',
            ],
            'type'  => 'manga',
        ];

        protected $authHandler;
        protected const BASE_URI = 'https://api.mangadex.org/';

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https:\/\/(?:www\.)?(mangadex\.org\/title\/(?:\w{8}-\w{4}-\w{4}-\w{4}-\w{12}))(\/.*)?$/", $link, $m))
                return 'https://'.$m[1];

            return null;
        }

        protected function auth()
        {
            if (!empty($this->authHandler))
                return;

            $authStrategy = new JsonAuthStrategy([
                'username' => Config::config('MANGADEX_USERNAME'),
                'password' => Config::config('MANGADEX_PASSWORD'),
                'json_fields' => ['username', 'password'],
            ]);

            $cache = new Psr16Cache(new FilesystemAdapter());
            $persistenceStrategy = new SimpleCacheTokenPersistence($cache);

            $authClient = new CurlClient(['base_uri' => self::BASE_URI]);
            
            $jwtManager = new JwtManager(
                $authClient,
                $authStrategy,
                $persistenceStrategy,
                [
                    'token_url' => '/auth/login',
                    'token_key' => 'token.session',
                    //'expire_key' => 'token.refresh',
                ]
            );

            // Create a HandlerStack
            $this->authHandler = HandlerStack::create();

            // Add middleware
            $this->authHandler->push(new JwtMiddleware($jwtManager));
        }

        public function get_latest_version(string $link): ?object
        {
            if (preg_match("/^https:\/\/(?:www\.)?mangadex\.org\/title\/(\w{8}-\w{4}-\w{4}-\w{4}-\w{12})$/", $link, $m))
                $id = $m[1];
            else
                return null;
            
            $storage = \Storage\StorageFactory::get_storage();
            $storage->con_ping(true);
            $link_data = $storage->get_link_data($link);

            $this->auth();
            $client = new Client(['handler' => $this->authHandler, 'base_uri' => self::BASE_URI]);
            
            // manga data first
            $response = $client->get('/manga/' . $id);
            $manga_data = json_decode($response->getBody());
            if (empty($manga_data->result) || $manga_data->result != 'ok')
                return null;
            $name = $manga_data->data->attributes->title->en ?? '';
            if (empty($name)) {
                foreach ($manga_data->data->attributes->title as $title) {
                    $name = $title;
                    break;
                }
            }
            if (empty($name))
                return null;

            // chapters
            $params = [
                'manga' => $id,
                'order' => [
                    'publishAt' => 'desc',
                ],
            ];
            if (!empty($link_data->last_version_ts)) {
                $dt = new \DateTime('now', new \DateTimeZone('UTC'));
                $dt->setTimestamp($link_data->last_version_ts);
                $params['publishAtSince'] = $dt->format('Y-m-d\TH:i:s');
            }
            $response = $client->get('/chapter?' . http_build_query($params));
            $chapters_data = json_decode($response->getBody());
            if (empty($chapters_data->result) || $chapters_data->result != 'ok' || empty($chapters_data->data))
                return null;
            $chapter_names = [];
            foreach ($chapters_data->data as $chapter) {
                if ($chapter->type != 'chapter')
                    continue;
                $chapter_names[] = trim(
                    (!empty($chapter->attributes->translatedLanguage) ? ('[' . $chapter->attributes->translatedLanguage . '] ') : '') .
                    (!empty($chapter->attributes->volume) ? ('Volume: ' . $chapter->attributes->volume . ' ') : '') .
                    (!empty($chapter->attributes->chapter) ? ('Chapter: ' . $chapter->attributes->chapter . ' ') : '') .
                    (!empty($chapter->attributes->title) ? $chapter->attributes->title : '')
                );
            }
            if (empty($chapter_names))
                return null;
            
            $last_ep = implode("\n", $chapter_names);
            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $last_ep,
            ];
            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($last_ep),
            ];
        }
    }
