<?php
    namespace Parser;

    use PHPHtmlParser\Dom;
    use PHPHtmlParser\Options;

    class ParserRutracker extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'rutracker.org' => 'https://rutracker.org/forum/viewtopic.php?t=<ID>',
            ],
            'type'  => 'torrent',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https:\/\/(rutracker\.org\/forum\/viewtopic\.php\?t=\d+)/", $link, $m))
                return 'https://'.$m[1];

            if (preg_match("/^https:\/\/(rutracker\.org\/forum\/viewtopic\.php\?).*&(t=\d+)/", $link, $m))
                return 'https://'.$m[1].$m[2];

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $page = @file_get_contents($link);
            if (empty($page))
                return null;
            $page = iconv('cp1251', 'utf-8//IGNORE', $page);

            $options = new Options();
            $options->setEnforceEncoding('utf-8');
            $dom = new Dom();
            $dom->loadStr($page, $options);

            $name = @trim($dom->find('span[style=font-size: 24px; line-height: normal;]')[0]->text);
            if (empty($name))
                $name = @trim($dom->find('a#topic-title')[0]->text);
            if (empty($name))
                return null;

            $magnet_link = '';
            $magnet_link_dom = @($dom->find('a.magnet-link')[0]);
            if (!empty($magnet_link_dom))
                $magnet_link = @trim($magnet_link_dom->getAttribute('href'));
            if (empty($magnet_link))
                return null;

            unset($dom);

            $mark = (object)[
                'name'          => $name,
                'magnet_link'   => $magnet_link,
            ];

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($magnet_link),
            ];
        }
    }
