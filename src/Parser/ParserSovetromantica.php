<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserSovetromantica extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'sovetromantica.com' => 'https://sovetromantica.com/anime/<ID>-<NAME>',
            ],
            'type'  => 'anime',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https:\/\/(sovetromantica.com\/(?:anime|dorama)\/\d+)[_-]([^\/]+)(?:\/.*)?$/", $link, $m))
                return 'https://'.$m[1].'-'.$m[2];

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim($dom->find('div.anime-name div.block--container')[0]->text);
            if (empty($name))
                return null;

            $episodes = @($dom->find('div.episodes-slick_item a span'));
            if (!empty($episodes)) {
                $latest_chapter_name = trim($episodes[count($episodes) - 1]->text);
                if (preg_match("/#(\d+)\s*$/", $latest_chapter_name, $m))
                    $latest_chapter_ep_num = $m[1];
            }
            
            if (empty($latest_chapter_name))
                $latest_chapter_name = '';

            $completed = false;
            $items = $dom->find('ul.anime-info_block li');
            if (!empty($items)) {
                foreach ($items as $item) {
                    if ($item->text === 'Кол-во эпизодов') {
                        $episodes_count = $item->find('span')->text;
                        if (!empty($episodes_count) && $episodes_count == $latest_chapter_ep_num) {
                            $completed = true;
                            break;
                        }
                    }
                }
            }

            unset($dom);

            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $latest_chapter_name,
            ];
            if ($completed)
                $mark->completed = 1;

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($latest_chapter_name),
            ];
        }
    }
