<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserMangalib extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                'mangalib.me'   => 'https://mangalib.me/<NAME>',
                'ranobelib.me'  => 'https://ranobelib.me/<NAME>',
                'hentailib.me'  => 'https://hentailib.me/<NAME>',
            ],
            'type'  => 'manga',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https?:\/\/(mangalib\.me\/[^\/\?]+|ranobelib\.me\/[^\/\?]+|hentailib\.me\/[^\/\?]+)(?:\/v\d+\/c\d+\/.*)?(\?.*)?$/", $link, $m))
                return 'https://'.$m[1];

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim($dom->find('h1')[0]->text);
            if (empty($name))
                return null;

            $latest_chapter_name_part1 = @trim($dom->find('div.chapter-item__name a')[0]->text);
            $latest_chapter_name_part2 = @trim($dom->find('div.chapter-item__name a span')[0]->text);
            $latest_chapter_name = $latest_chapter_name_part1.' '.$latest_chapter_name_part2;
            if (empty($latest_chapter_name))
                $latest_chapter_name = '';
            else
                $latest_chapter_name = trim(preg_replace("/\s+/", ' ', $latest_chapter_name));

            $completed = false;
            if ($sp = $dom->find('span.m-label_success')) {
                if (count($sp) && $sp->text == 'завершен')
                    $completed = true;
            }

            unset($dom);

            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $latest_chapter_name,
            ];
            if ($completed)
                $mark->completed = 1;

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($latest_chapter_name),
            ];
        }
    }
