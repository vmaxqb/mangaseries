<?php
    namespace Parser;

    use CurlClient\CurlClient;
    use PHPHtmlParser\Dom;

    class ParserManganelo extends AbstractParser
    {
        public static $manifest = [
            'host'  => [
                //'manganelo.com'     => 'https://manganelo.com/manga/<NAME>',
                'mangakakalot.com'  => 'https://mangakakalot.com/manga/<NAME>',
                'manganato.com'     => 'https://manganato.com/manga-<ID>',
                'readmanganato.com' => 'https://readmanganato.com/manga-<ID>',
            ],
            'type'  => 'manga',
        ];

        public static function verify_link(string $link): ?string
        {
            $link = trim($link);

            if (preg_match("/^https?:\/\/((?:manganelo|mangakakalot)\.com\/manga\/[^\/]+)$/", $link, $m))
                return 'https://'.$m[1];

            if (preg_match("/^https?:\/\/(manganelo|mangakakalot)\.com\/chapter\/([^\/]+)\/[^\/]+$/", $link, $m))
                return 'https://'.$m[1].'.com/manga/'.$m[2];

            if (preg_match("/^https?:\/\/((?:read)?manganato\.com\/manga-[^\/]+)(\/(chapter-.*)?)?$/", $link, $m))
                return 'https://'.$m[1];

            return null;
        }


        public function get_latest_version(string $link): ?object
        {
            $dom = new Dom();
            try {
                $dom->loadFromUrl($link, null, new CurlClient());
            } catch (\Exception $e) {
                return null;
            }

            $name = @trim($dom->find('h1')[0]->text);
            if (empty($name))
                return null;

            $latest_chapter_name = @($dom->find('div.panel-story-chapter-list a.chapter-name')[0]->text);
            if (empty($latest_chapter_name)) {
                $latest_chapter_name = $dom->find('div.chapter-list .row span a')[0]->text;
            }

            $completed = false;
            foreach ($dom->find('table.variations-tableInfo td.table-value') as $td) {
                if ($td->text == 'Completed')
                    $completed = true;
            }

            unset($dom);

            $mark = (object)[
                'name'      => $name,
                'last_ep'   => $latest_chapter_name,
            ];
            if ($completed)
                $mark->completed = 1;

            return (object)[
                'mark'  => $mark,
                'hash'  => sha1($latest_chapter_name),
            ];
        }
    }
