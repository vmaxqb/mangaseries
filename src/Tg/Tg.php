<?php
    namespace Tg;

    /**
     * This class realizes some telegram functions
     */
    class Tg
    {
        /**
         * @var \CurlHandle Curl connection, returned by curl_init
         */
        private $curl_con;

        /**
         * @var int Messages, bigger than this value will be split to several messages, lesser than MAX_MESSAGE_SIZE each
         */
        const MAX_MESSAGE_SIZE = 4096;

        /**
         * Makes curl connection right away
         */
        public function __construct()
        {
            $this->curl_con = curl_init();

            curl_setopt_array($this->curl_con, [
                CURLOPT_HEADER          => false,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_POST            => true,
            ]);
        }

        /**
         * Makes a correct URL for telegram API
         * @param string $method The method API will use
         * @return string URL for telegram API
         */
        private function get_api_url(string $method): string
        {
            return 'https://api.telegram.org/bot'.\Config\Config::config('TG_TOKEN').'/'.$method;
        }

        /**
         * Making basic "lowlevel" query. May be used by methods making higher level queries
         * @param string $method The method API will use
         * @param array $data Parameters of the query
         * @param bool $upload_file Are we uploading a file?
         * @return object|null The answer of telegram API. Null on curl error
         */
        private function api_basic_query(string $method, array $data, bool $upload_file=false): ?object
        {
            if (!$upload_file) {
                $opts = [
                    CURLOPT_URL             => $this->get_api_url($method),
                    CURLOPT_POSTFIELDS      => json_encode($data),
                    CURLOPT_HTTPHEADER      => [
                        'Content-Type: application/json; charset=utf-8',
                    ],
                ];
            } else {
                $opts = [
                    CURLOPT_URL             => $this->get_api_url($method),
                    CURLOPT_POSTFIELDS      => $data,
                ];
            }

            curl_setopt_array($this->curl_con, $opts);
            $res = curl_exec($this->curl_con);

            $curl_error = curl_error($this->curl_con);
            
            if ($curl_error)
                return null;
            
            return @json_decode($res);
        }

        /**
         * This method makes a http-answer to telegram "update".
         * Used when we are webhooked
         * @param string $method The method API will use
         * @param array $data Parameters of the query
         */
        private function api_basic_answer(string $method, array $data): void
        {
            $data['method'] = $method;

            header('Content-Type: application/json; charset=utf-8');
            echo(json_encode($data));
            die;
        }

        /**
         * Sends a text to the user (chat).
         * Splits the text by parts. Every part length is less than self::MAX_MESSAGE_SIZE
         * @param int $chat_id telegram chat_id
         * @param string $text The text you want to send
         * @param array $params Extra or non-default parameters
         */
        public function send_text_message(int $chat_id, string $text, array $params=[]): void
        {
            $text_parts = [];
            $text_lines = explode("\n", $text);

            $current_part_len = 0;
            $current_part = '';
            foreach ($text_lines as $line) {
                $len = strlen($line) + 1;
                if ($current_part_len + $len < self::MAX_MESSAGE_SIZE) {
                    $current_part_len += $len;
                    $current_part .= $line."\n";
                }
                else {
                    $text_parts[] = $current_part;
                    $current_part_len = $len;
                    $current_part = $line."\n";
                }
            }
            $text_parts[] = $current_part;

            foreach ($text_parts as $text_part) {
                $tlgm_data = $params + [
                    'chat_id'                   => $chat_id,
                    'text'                      => $text_part,
                    'parse_mode'                => 'HTML',
                    'disable_web_page_preview'  => true,
                ];
                $this->api_basic_query('sendMessage', $tlgm_data);
            }
        }

        /**
         * Making a http-answer to telegram "update".
         * If the $text is bigger than maximum length, we will send text by API-query, not an answer
         * @param int $chat_id telegram chat_id
         * @param string $text The text you want to send
         * @param array $params Extra or non-default parameters
         */
        public function answer_text(int $chat_id, string $text, array $params=[]): void
        {
            if ($text < self::MAX_MESSAGE_SIZE) {
                $tlgm_data = $params + [
                    'chat_id'                   => $chat_id,
                    'text'                      => $text,
                    'parse_mode'                => 'HTML',
                    'disable_web_page_preview'  => true,
                ];
                $this->api_basic_answer('sendMessage', $tlgm_data);
            } else
                $this->send_text_message($chat_id, $text, $params);
        }

        /**
         * Gets contents of a telegram "update"
         * Also getting some most usable data of update easier to get
         * @return object Update
         */
        public function get_update_contents(): object
        {
            $secret = \Config\Config::config('TG_SECRET');
            if (!empty($secret) && !isset($_GET[$secret]))
                die('wrong secret');

            $content = @file_get_contents("php://input");
            if (empty($content))
                die('empty content');

            $update = @json_decode($content);
            if (empty($update))
                die('empty update');

            // some basic parsing
            $basic_params = new \stdClass;

            if (!isset($update->callback_query)) {
                if (empty($update->message->chat->id)) {
                    //debug_msg('empty chat id');
                    die('empty chat id');
                }
                $basic_params->chat_id = $update->message->chat->id;
                $basic_params->msg_id = $update->message->message_id;
                $basic_params->reply_msg_id = 0;
                if (!empty($update->message->reply_to_message->message_id))
                    $basic_params->reply_msg_id = $update->message->reply_to_message->message_id;
                $basic_params->reply_to_user_id = 0;
                if (!empty($update->message->reply_to_message->from->id))
                    $basic_params->reply_to_user_id = $update->message->reply_to_message->from->id;
                $basic_params->reply_to_me = ($basic_params->reply_to_user_id == \Config\Config::config('TG_SELF_USER_ID'));
                $basic_params->from = $update->message->from;
                $basic_params->msg_text = trim($update->message->text);
            }
            else {
                if (empty($update->callback_query->message->chat->id)) {
                    //debug_msg('empty chat id');
                    die('empty chat id');
                }
                $basic_params->chat_id = $update->callback_query->message->chat->id;
                $basic_params->msg_id = $update->callback_query->message->message_id;
                $basic_params->reply_msg_id = $basic_params->msg_id;
                $basic_params->reply_to_user_id = 0;
                if (!empty($update->callback_query->message->from->id))
                    $basic_params->reply_to_user_id = $update->callback_query->message->from->id;
                $basic_params->reply_to_me = ($basic_params->reply_to_user_id == \Config\Config::config('TG_SELF_USER_ID'));
                $basic_params->from = $update->callback_query->from;
                $basic_params->msg_text = isset($update->callback_query->message->text)?trim($update->callback_query->message->text):'';
                $basic_params->msg_inline_data = trim($update->callback_query->data);
            }

            return (object)[
                'update'        => $update,
                'basic_params'  => $basic_params,
            ];
        }

        /**
         * Sets a webhook
         * @return object|null Decoded API answer. Null on error
         */
        public function set_webhook(): ?object
        {
            $cert_file = \Config\Config::config('TG_CERT');
            $webhook_url = \Config\Config::config('TG_WEBHOOK');
            $secret = \Config\Config::config('TG_SECRET');

            if (!empty($secret))
                $webhook_url .= '?'.$secret;

            $curl_cert_file = curl_file_create($cert_file);

            $tlgm_data = [
                'url'           => $webhook_url,
                'certificate'   => $curl_cert_file,
            ];

            $tlgm_res = $this->api_basic_query('setWebhook', $tlgm_data, true);

            return $tlgm_res;
        }

    }
