<?php
    namespace Controller;

    class ControllerListSites extends AbstractController
    {
        public function handle(): object
        {
            $new_state = (object)[
                'state' => '',
                'data'  => '',
            ];

            $ext_id = $this->update->basic_params->chat_id;
            $msg_text = $this->update->basic_params->msg_text;

            $parser_factory = \Parser\ParserFactory::getInstance();
            $storage = \Storage\StorageFactory::get_storage();

            $user_id = $storage->get_user_id('tg', $ext_id);

            $answer_parts = [];
            $mainfests = $parser_factory->get_all_manifests();
            foreach ($mainfests as $type => $sites) {
                $answer_parts[] = '<i>'.ucwords($type).':</i>'."\n".implode("\n", $sites);
            }

            return (object)[
                'state'     => $new_state,
                'answer'    => implode("\n\n", $answer_parts),
            ];
        }
    }
