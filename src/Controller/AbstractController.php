<?php
    namespace Controller;

    /**
     * This class is basic for all controllers, classes that process telegram queries
     */
    abstract class AbstractController
    {
        /**
         * @var object tg-query
         */
        protected $update;
        /**
         * @var object|empty-string current state of this tg-user
         */
        protected $state;

        public function __construct(object $update, $state)
        {
            $this->update = $update;
            $this->state = $state;
        }
        
        /**
         * This method handles a certain tg-query
         * @return object Returns new user state (object) and an answer to user (array|string)
         *      If answer is a string - it will be sent to user by \Tg\Tg::answer_text function
         *      If answer is an array - it should be valid for using in \Tg\Tg::api_basic_answer function
         *      Example: (object)[
         *          'state'     => (object)[
         *              'state' => (string)
         *              'data'  => (object|empty-string)
         *          ],
         *          'answer'    => (array|string)
         *      ]
         */
        abstract public function handle(): object;
    }
