<?php
    namespace Controller;

    use \Lang\L;

    class ControllerAddLinks extends AbstractController
    {
        public function handle(): object
        {
            $new_state = (object)[
                'state' => '',
                'data'  => '',
            ];

            $ext_id = $this->update->basic_params->chat_id;
            $msg_text = $this->update->basic_params->msg_text;

            $parser_factory = \Parser\ParserFactory::getInstance();
            $storage = \Storage\StorageFactory::get_storage();

            $user = $storage->get_user('tg', $ext_id);
            L::set_lang($user->lang);

            $parts = preg_split("/\s+/", $msg_text);
            $parts = array_unique($parts);
            $links = [];
            $answer_parts = [];
            foreach ($parts as $link) {
                $link = trim($link);
                if (!preg_match("/^https?:\/\//", $link))
                    continue;
                if (!filter_var($link, FILTER_VALIDATE_URL))
                    continue;
                $link_allowed = $parser_factory->is_link_allowed($link);
                if ($link_allowed->is_allowed) {
                    $link = $link_allowed->correct_link;
                    $link_id = $storage->get_link_id($link);
                    if (!$link_id)
                        continue;
                    $storage->add_user_link($user->user_id, $link_id);
                    $answer_parts[] = $link."\n".L::_('LINK_ADDED', '/del_ul'.$link_id);;
                } elseif (!empty($link_allowed->correct_link))
                    $answer_parts[] = $link."\n".L::_('LINK_INCORRECT_FORMAT', htmlspecialchars($link_allowed->correct_link));
                else {
                    $storage->add_suggest($user->user_id, $link);
                    $answer_parts[] = $link."\n".L::_('LINK_UNKNOWN');
                }
            }

            if (empty($answer_parts))
                $answer_parts[] = L::_('NO_LINKS_DETECTED');

            return (object)[
                'state'     => $new_state,
                'answer'    => implode("\n\n", $answer_parts),
            ];
        }
    }
