<?php
    namespace Controller;

    use \Lang\L;

    class ControllerDeleteLink extends AbstractController
    {
        public function handle(): object
        {
            $new_state = (object)[
                'state' => '',
                'data'  => '',
            ];

            $ext_id = $this->update->basic_params->chat_id;
            $msg_text = $this->update->basic_params->msg_text;

            $parser_factory = \Parser\ParserFactory::getInstance();
            $storage = \Storage\StorageFactory::get_storage();

            $user = $storage->get_user('tg', $ext_id);
            L::set_lang($user->lang);

            $answer = '';
            if (preg_match("/^\/del_ul(\d+)/", $msg_text, $m)) {
                $link_id = $m[1];
                $storage->del_user_link($user->user_id, $link_id);
                $answer = L::_('DELETED');
            }

            return (object)[
                'state'     => $new_state,
                'answer'    => $answer,
            ];
        }
    }
