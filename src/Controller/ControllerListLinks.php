<?php
    namespace Controller;

    use \Lang\L;

    class ControllerListLinks extends AbstractController
    {
        public function handle(): object
        {
            $new_state = (object)[
                'state' => '',
                'data'  => '',
            ];

            $ext_id = $this->update->basic_params->chat_id;
            $msg_text = $this->update->basic_params->msg_text;

            $parser_factory = \Parser\ParserFactory::getInstance();
            $storage = \Storage\StorageFactory::get_storage();

            $user = $storage->get_user('tg', $ext_id);
            L::set_lang($user->lang);

            $links_data = $storage->get_user_links($user->user_id);

            $answer_parts = [];
            foreach ($links_data as $link_data) {
                if (!empty($link_data->last_version_mark->name)) {
                    $answer_parts[] = '<a href="'.$link_data->link.'">'.htmlspecialchars($link_data->last_version_mark->name).'</a>'."\n".
                        L::_('DELETE_LINK', '/del_ul'.$link_data->link_id);
                        
                } else {
                    $answer_parts[] = $link_data->link."\n".
                        L::_('DELETE_LINK', '/del_ul'.$link_data->link_id);
                }
            }

            return (object)[
                'state'     => $new_state,
                'answer'    => implode("\n\n", $answer_parts),
            ];
        }
    }
