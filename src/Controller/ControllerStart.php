<?php
    namespace Controller;

    use \Lang\L;

    class ControllerStart extends AbstractController
    {
        public function handle(): object
        {
            $new_state = (object)[
                'state' => '',
                'data'  => '',
            ];

            $ext_id = $this->update->basic_params->chat_id;
            $msg_text = $this->update->basic_params->msg_text;

            $storage = \Storage\StorageFactory::get_storage();

            $user = $storage->get_user('tg', $ext_id);
            L::set_lang($user->lang);

            return (object)[
                'state'     => $new_state,
                'answer'    => L::_('START_MSG'),
            ];
        }
    }
