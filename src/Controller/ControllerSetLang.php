<?php
    namespace Controller;

    use \Lang\L;

    class ControllerSetLang extends AbstractController
    {
        public function handle(): object
        {
            $new_state = (object)[
                'state' => '',
                'data'  => '',
            ];

            $ext_id = $this->update->basic_params->chat_id;
            $msg_text = $this->update->basic_params->msg_text;

            $parser_factory = \Parser\ParserFactory::getInstance();
            $storage = \Storage\StorageFactory::get_storage();

            $user = $storage->get_user('tg', $ext_id);
            L::set_lang($user->lang);

            $answer = '';
            if (preg_match("/^\/lang_(\w+)/", $msg_text, $m)) {
                $lang = $m[1];
                if (in_array($lang, L::get_available_langs())) {
                    $storage->set_user_lang($user->user_id, $lang);
                    L::set_lang($lang);
                    $answer = L::_('LANG_SET');
                } else
                    $answer = L::_('UNKNOWN_LANG');
            } else
                $answer = L::_('UNKNOWN_LANG');

            return (object)[
                'state'     => $new_state,
                'answer'    => $answer,
            ];
        }
    }
