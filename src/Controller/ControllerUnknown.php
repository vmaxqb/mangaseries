<?php
    namespace Controller;

    use \Lang\L;

    class ControllerUnknown extends AbstractController
    {
        public function handle(): object
        {
            $new_state = (object)[
                'state' => '',
                'data'  => '',
            ];

            $ext_id = $this->update->basic_params->chat_id;

            $storage = \Storage\StorageFactory::get_storage();

            $user = $storage->get_user('tg', $ext_id);
            L::set_lang($user->lang);

            return (object)[
                'state'     => $new_state,
                'answer'    => L::_('UNKNOWN_COMMAND'),
            ];
        }
    }
