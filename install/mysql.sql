SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `link_id` bigint(20) NOT NULL,
  `link` varchar(2048) DEFAULT NULL,
  `last_version_mark` varchar(5000) DEFAULT NULL,
  `last_version_hash` varchar(40) DEFAULT NULL,
  `last_version_dt` timestamp NULL DEFAULT NULL,
  `completed` tinyint(4) NOT NULL DEFAULT '0',
  `last_checked_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `suggests`;
CREATE TABLE `suggests` (
  `suggest_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `link` varchar(2048) NOT NULL,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `tg_user_state`;
CREATE TABLE `tg_user_state` (
  `ext_id` bigint(20) NOT NULL,
  `state` varchar(25) NOT NULL,
  `dt_message` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL,
  `platform` set('tg','mail') NOT NULL,
  `ext_id` varchar(100) NOT NULL,
  `tz_delta` tinyint(4) NOT NULL DEFAULT '5',
  `lang` varchar(5) NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `user_links`;
CREATE TABLE `user_links` (
  `user_link_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `link_id` bigint(20) NOT NULL,
  `last_version_hash` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link` (`link`(100)) USING BTREE;

ALTER TABLE `suggests`
  ADD PRIMARY KEY (`suggest_id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `tg_user_state`
  ADD PRIMARY KEY (`ext_id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `platform_ext_id` (`platform`,`ext_id`);

ALTER TABLE `user_links`
  ADD PRIMARY KEY (`user_link_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `link_id` (`link_id`);


ALTER TABLE `links`
  MODIFY `link_id` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `suggests`
  MODIFY `suggest_id` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `users`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `user_links`
  MODIFY `user_link_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user_links`
  ADD CONSTRAINT `user_links_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_links_ibfk_2` FOREIGN KEY (`link_id`) REFERENCES `links` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
