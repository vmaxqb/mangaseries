<?php
    require __DIR__.'/../../vendor/autoload.php';

    file_put_contents('redirects.txt', '');

    $parser_factory = \Parser\ParserFactory::getInstance();
    $storage = \Storage\StorageFactory::get_storage();

    $links_data = $storage->get_used_links();

    $curl_con = curl_init();
    curl_setopt_array($curl_con, [
        CURLOPT_FOLLOWLOCATION  => true,
        CURLOPT_HEADER          => true,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_NOBODY          => true,
    ]);

    foreach ($links_data as $link_data) {
        curl_setopt($curl_con, CURLOPT_URL, $link_data->link);
        $result = curl_exec($curl_con);
        $code = curl_getinfo($curl_con, CURLINFO_HTTP_CODE);
        $redir_link = curl_getinfo($curl_con, CURLINFO_EFFECTIVE_URL);
        if ($redir_link == $link_data->link)
            continue;
        file_put_contents('redirects.txt', $link_data->link."\t".$redir_link."\n", FILE_APPEND);
    }
