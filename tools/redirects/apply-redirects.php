<?php
    require __DIR__.'/../../vendor/autoload.php';

    function change_log(string $msg): void
    {
        file_put_contents('log.txt', date('Y-m-d h:i:s :: ').$msg."\n", FILE_APPEND);
    }

    function get_users_of_link(int $link_id): array
    {
        $con = \DbConnection\DbConnection::connect();

        $sql = "
            select distinct
                ul.user_id
            from
                user_links ul
            where
                ul.link_id = ?
            order by
                ul.user_id
        ";
        $stmt = $con->prepare($sql);
        $stmt->bind_param('i', $link_id);
        $stmt->execute();
        $result = [];
        foreach (\DbConnection\DbConnection::fetch_results($stmt) as $row) {
            $result[] = $row->user_id;
        }
        return $result;
    }

    $storage = \Storage\StorageFactory::get_storage();
    $con = \DbConnection\DbConnection::connect();

    $redirects = file_get_contents('redirects.txt');
    $redirects = explode("\n", $redirects);

    foreach ($redirects as $redirect) {
        $redirect = trim($redirect);
        if (empty($redirect))
            continue;
        list($old_link, $new_link) = explode("\t", $redirect);
        $old_link = trim($old_link);
        $new_link = trim($new_link);
        if (empty($old_link) || empty($new_link))
            continue;
        
        $old_link_id = $storage->get_link_id($old_link, false);
        if (empty($old_link_id))
            continue;
        
        // do we already have a new link in our db?
        $new_link_id = $storage->get_link_id($new_link, false);
        $new_link_exists = !empty($new_link_id);
        if (!$new_link_exists) {
            $sql = "
                update
                    links l
                set
                    l.link = ?
                where
                    l.link_id = ?
            ";
            $stmt = $con->prepare($sql);
            $stmt->bind_param('si', $new_link, $old_link_id);
            $stmt->execute();
            change_log('Changed link '.$old_link_id.' ('.$old_link.' -> '.$new_link.')');
        } else {
            $users_of_old_link = get_users_of_link($old_link_id);
            $users_of_new_link = get_users_of_link($new_link_id);

            foreach ($users_of_old_link as $user_id) {
                if (in_array($user_id, $users_of_new_link)) {
                    // user uses both old and new link. delete the old one in this case
                    $storage->del_user_link($user_id, $old_link_id);
                    change_log('Deleted old user '.$user_id.' link '.$old_link_id.' ('.$old_link.') as he has new link '.$new_link_id.' ('.$new_link.')');
                } else {
                    $sql = "
                        update
                            user_links ul
                        set
                            ul.link_id = ?
                        where
                            ul.link_id = ? and
                            ul.user_id = ?
                    ";
                    $stmt = $con->prepare($sql);
                    $stmt->bind_param('iii', $new_link_id, $old_link_id, $user_id);
                    $stmt->execute();
                    change_log('Changed user '.$user_id.' link '.$old_link_id.' -> '.$new_link_id.' ('.$old_link.' -> '.$new_link.')');
                }
            }
        }
    }


