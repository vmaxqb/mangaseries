<?php
    require __DIR__.'/vendor/autoload.php';

    use \Lang\L;

    $tg = new \Tg\Tg();
    $storage = \Storage\StorageFactory::get_storage();

    $links_data = $storage->get_nonrenewed_user_links('tg');

    $posts = [];
    $need_updates = [];
    foreach ($links_data as $link_data) {
        L::set_lang($link_data->lang);

        $need_updates[$link_data->user_id][] = $link_data->link_id;

        if (!isset($posts[$link_data->ext_id]))
            $posts[$link_data->ext_id] = '';

        if (!empty($link_data->last_version_mark->name))
            $post = '<a href="'.$link_data->link.'">'.htmlspecialchars($link_data->last_version_mark->name).'</a>';
        else
            $post = $link_data->link;

        $post .= "\n".L::_('NEW_EPISODE_OUT');
        if (!empty($link_data->last_version_mark->last_ep))
            $post .= ': '.$link_data->last_version_mark->last_ep;

        if (!empty($link_data->last_version_mark->lang))
            $post .= "\n".L::_('EPISODE_LANG').': '.$link_data->last_version_mark->lang;

        if (!empty($link_data->completed))
            $post .= "\n".L::_('END_EPISODE');

        $posts[$link_data->ext_id] .= $post."\n\n";
    }

    // notifying
    foreach ($posts as $ext_id => $post) {
        $tg->send_text_message($ext_id, trim($post), ['disable_web_page_preview' => false]);
    }

    // updating
    foreach ($need_updates as $user_id => $link_ids) {
        foreach ($link_ids as $link_id)
            $storage->renew_user_link($user_id, $link_id);
    }
