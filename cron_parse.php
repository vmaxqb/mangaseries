<?php
    require __DIR__.'/vendor/autoload.php';

    $max_proc_count = \Config\Config::config('PARSER_MAX_PROC_COUNT', 5);

    $parser_factory = \Parser\ParserFactory::getInstance();
    $storage = \Storage\StorageFactory::get_storage();

    $pre_links_data = $storage->get_used_links();

    $links_data = [];
    foreach ($pre_links_data as $link_data) {
        // should we really check this link?
        $do_check = false;
        
        if (empty($link_data->ts_since_update) || empty($link_data->ts_since_check))
            $do_check = true;
        else {
            // days since last_update
            $d_since_update = $link_data->ts_since_update / 60 / 60 / 24;
            // how frequently can we check, in hours 
            if ($d_since_update < 3)
                $h_since_check_limit = 1;
            elseif ($d_since_update < 10)
                $h_since_check_limit = 2;
            elseif ($d_since_update < 30)
                $h_since_check_limit = 4;
            elseif ($d_since_update < 60)
                $h_since_check_limit = 12;
            elseif ($d_since_update < 150)
                $h_since_check_limit = 24;
            else
                $h_since_check_limit = 48;

            if (round($link_data->ts_since_check / 60 / 60) >= $h_since_check_limit)
                $do_check = true;
        }

        if ($do_check)
            $links_data[] = $link_data;
    }

    $cur_proc_count = 0;
    $all_proc_ended = false;
    while (!$all_proc_ended) {
        while ($cur_proc_count < $max_proc_count && !empty($links_data)) {
            $link_data = array_shift($links_data);
            $pid = pcntl_fork();
            if ($pid == -1)
                die('No fork available');
            elseif ($pid > 0)
                $cur_proc_count++;
            elseif ($pid == 0) {
                $parser = $parser_factory->get_parser($link_data->link);
                if (!$parser)
                    die;

                $lv_result = '';
                try {
                    $lv_result = @($parser->get_latest_version($link_data->link));
                } catch (Exception $e) {}

                $storage->con_ping(true);

                if (empty($lv_result) || ($link_data->last_version_hash == $lv_result->hash && empty($lv_result->mark->completed)))
                    $storage->check_link($link_data->link_id);
                else 
                    $storage->renew_link($link_data->link_id, $lv_result->mark, $lv_result->hash);

                die;
            }
        }

        if ($cur_proc_count > 0) {
            $wait_res = pcntl_wait($status);
            $cur_proc_count--;
        } else
            $all_proc_ended = true;
    }
